/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z1;

/**
 *
 * @author Mladen
 */
public class EggClassification {
    
    public int classify(int mass, int diameter, boolean deformation){
        int eggClass = 0;
        
        if((mass >= 50) && (diameter >2)){
            eggClass = 1;
        }
        else if((mass <=50) || (diameter <=2)){
            eggClass = 2;
        }
        else if((mass<=50) && (diameter <=2)){
            eggClass = 3;
        }
        
        if(deformation == true){
            eggClass +=1;
        }
        
        return eggClass;
    }
    
   
    
}
