/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z2;

/**
 *
 * @author Mladen
 */
public class EggDelivery {
    
    public float calculatePrice(int numberOfEggs, int distance, int eggClass, boolean express){
        float price = 100/eggClass;
        
        if(distance > 75 && distance < 100){
            price *=2;
            if(express){
                price *=2;
            }
        }
        
        else if(distance >= 100){
            price *=3;
            if(express){
                price *=2;
            }
        }
        else{
            if(express){
                price *=2;
            }
        }
        
        if(price > 1000){
            price *=0.9;
        }
        
        return price * numberOfEggs;
    }
    
}
