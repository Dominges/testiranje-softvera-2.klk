/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z1;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Dominik
 */
public class EggClassificationTest {
    
    public EggClassificationTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of classify method, of class EggClassification.
     */
 @org.junit.Test
    public void testClassify1() {
        System.out.println("classify");
        int mass = 60;
        int diameter = 3;
        boolean deformation = false;
        EggClassification instance = new EggClassification();
        int expResult = 1;
        int result = instance.classify(mass, diameter, deformation);
        assertEquals(expResult, result);
        
    }
     @org.junit.Test
    public void testClassify2() {
        System.out.println("classify");
        int mass = 60;
        int diameter = 1;
        boolean deformation = false;
        EggClassification instance = new EggClassification();
        int expResult = 2;
        int result = instance.classify(mass, diameter, deformation);
        assertEquals(expResult, result);
        
    }
     @org.junit.Test
    public void testClassify3() {
        System.out.println("classify");
        int mass = 40;
        int diameter = 3;
        boolean deformation = false;
        EggClassification instance = new EggClassification();
        int expResult = 2;
        int result = instance.classify(mass, diameter, deformation);
        assertEquals(expResult, result);
        
    }
     @org.junit.Test
    public void testClassify4() {
        System.out.println("classify");
        int mass = 40;
        int diameter = 1;
        boolean deformation = false;
        EggClassification instance = new EggClassification();
        int expResult = 3;
        int result = instance.classify(mass, diameter, deformation);
        assertEquals(expResult, result);
        
    }
     @org.junit.Test
    public void testClassify5() {
        System.out.println("classify");
        int mass = 60;
        int diameter = 3;
        boolean deformation = true;
        EggClassification instance = new EggClassification();
        int expResult = 2;
        int result = instance.classify(mass, diameter, deformation);
        assertEquals(expResult, result);
        
    }
     @org.junit.Test
    public void testClassify6() {
        System.out.println("classify");
        int mass = 60;
        int diameter = 1;
        boolean deformation = true;
        EggClassification instance = new EggClassification();
        int expResult = 3;
        int result = instance.classify(mass, diameter, deformation);
        assertEquals(expResult, result);
        
    }
     @org.junit.Test
    public void testClassify7() {
        System.out.println("classify");
        int mass = 40;
        int diameter = 3;
        boolean deformation = true;
        EggClassification instance = new EggClassification();
        int expResult = 3;
        int result = instance.classify(mass, diameter, deformation);
        assertEquals(expResult, result);
        
    }
     @org.junit.Test
    public void testClassify8() {
        System.out.println("classify");
        int mass = 40;
        int diameter = 1;
        boolean deformation = true;
        EggClassification instance = new EggClassification();
        int expResult = 4;
        int result = instance.classify(mass, diameter, deformation);
        assertEquals(expResult, result);
        
    }
}
