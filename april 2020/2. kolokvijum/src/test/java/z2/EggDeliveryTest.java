/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Dominik
 */
public class EggDeliveryTest {
    
    public EggDeliveryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calculatePrice method, of class EggDelivery.
     */
    @Test
    public void testCalculatePrice1() {
        System.out.println("calculatePrice");
        int numberOfEggs = 1;
        int distance = 80;
        int eggClass = 2;
        boolean express = false;
        EggDelivery instance = new EggDelivery();
        float expResult = 100.0F;
        float result = instance.calculatePrice(numberOfEggs, distance, eggClass, express);
        assertEquals(expResult, result, 0.01);
        
    }
    @Test
    public void testCalculatePrice2() {
        System.out.println("calculatePrice");
        int numberOfEggs = 1;
        int distance = 80;
        int eggClass = 2;
        boolean express = true;
        EggDelivery instance = new EggDelivery();
        float expResult = 200.0F;
        float result = instance.calculatePrice(numberOfEggs, distance, eggClass, express);
        assertEquals(expResult, result, 0.01);
        
    }
    @Test
    public void testCalculatePrice3() {
        System.out.println("calculatePrice");
        int numberOfEggs = 2;
        int distance = 120;
        int eggClass = 1;
        boolean express = false;
        EggDelivery instance = new EggDelivery();
        float expResult = 600.0F;
        float result = instance.calculatePrice(numberOfEggs, distance, eggClass, express);
        assertEquals(expResult, result, 0.01);
        
    }
    @Test
    public void testCalculatePrice4() {
        System.out.println("calculatePrice");
        int numberOfEggs = 2;
        int distance = 120;
        int eggClass = 1;
        boolean express = true;
        EggDelivery instance = new EggDelivery();
        float expResult = 1200.0F;
        float result = instance.calculatePrice(numberOfEggs, distance, eggClass, express);
        assertEquals(expResult, result, 0.01);
        
    }
    
}
