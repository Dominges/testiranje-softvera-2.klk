/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author Dominik
 */
public class NewSeleneseIT {
    
    @Test
    public void testSimple() throws Exception {
       System.setProperty("webdriver.chrome.driver", "C:\\Users\\Dominik\\Desktop\\kolokvijumi\\april 2020\\2. kolokvijum\\web\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
         driver.get("C:\\Users\\Dominik\\Desktop\\kolokvijumi\\april 2020\\2. kolokvijum\\web\\zadatak.html");
        
        List<WebElement> red = driver.findElements(By.tagName("tr"));
        
        assertEquals(3, red.size()-1);
        
        WebElement id = driver.findElement(By.id("id"));
        WebElement kolicina = driver.findElement(By.id("kolicina"));
        WebElement cena = driver.findElement(By.id("cena"));
        WebElement dodaj = driver.findElement(By.id("dodaj"));
        
        id.clear();
        kolicina.clear();
        cena.clear();
        
        
        id.sendKeys("3");
        kolicina.sendKeys("6");
        cena.sendKeys("500");
        
        dodaj.click();
        
        List<WebElement> novired = driver.findElements(By.tagName("tr"));
        assertEquals(4, novired.size()-1); 
        WebElement brisi = driver.findElement(By.id("brisi"));
        brisi.click();
         
        List<WebElement> novired1 = driver.findElements(By.tagName("tr"));
        assertEquals(3, novired1.size()-1);
        
        WebElement filter = driver.findElement(By.id("filter"));
        filter.clear();
        filter.sendKeys("K123");
        
        
        List<WebElement> novired2 = driver.findElements(By.tagName("tr"));
        assertEquals(1, novired2.size()-1);  
       
        driver.quit();
    }
    
}
